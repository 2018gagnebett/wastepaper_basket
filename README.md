Découpage en sprint/jour et fonctionnalités :
    
    - Lundi aprem :Premier modèle simple sur Python
                    * Simulation de la chute libre d'un objet sphérique
                    * Affichage de la trajectoire
                    * Affichage de la cible d'atterrisage
                    * Obtention du MVP
    
    - Mercredi :Représentation sur Tkinter
                    * Ajout d'une interface pour faire varier les conditions initiales (v_0, theta_0...)
                    * Ajout d'une interface pour faire varier les les paramètres (masse, gravité...)
                    * Prise en compte des parois et des rebonds
                 
    - Jeudi :Optimisation du modèle 
                    * Implémentation de l'interface Pygame
                    * Ajout de frottements
                    * Optimisation du code Python
    
    - Vendredi : * Finalisation du projet
                 * Finalisation de l'interface (ajout d'options...)
                 * Préparation de la soutenance
                 

Répartition du travail :
    * Modèle physique : Alexis/Maxime/Shunan
    * Animation : Gabriel/Tom/ Anaïs

#Description des modules:
    * donnee_trajectoire fait les calculs de trajectoires à partir des modèles physiques
    * affichage_pygame crée une fenêtre de jeu avec Pygame
    * affichage_tkinter crée une fenêtre Tkinter pour entrer les paramètres puis lance le jeu
