from tkinter import *
from wastepaper_basket.affichage_pygame import open_initial_window

def create_main_window():
    main_window = Tk()
    main_window.title("Wastepaper Basket")

    # Définition des variables
    masse_str_var = StringVar()
    masse_str_var.set("1")   #masse en kg
    nom_corps_var = StringVar()
    nom_corps_var.set("Terre")
    pos_x_corbeille_var = DoubleVar()
    pos_x_corbeille_var.set("100")
    intensite_pesanteur_var = StringVar()
    intensite_pesanteur_var.set("9.81")
    dic_planete_pesanteur = {"Mercure": 3.7, "Venus": 8.87, "Terre": 9.81, "Mars": 3.71, "Jupiter": 24.79,
                             "Saturne": 10.44, "Uranus": 8.87, "Neptune":11.15, "Pluton":0.62, "Lune":1.62,
                             "Soleil":275, "TRAPPIST-1":1687}
    frottements_var = IntVar()
    frottements_var.set(0)
    nom_milieu_var = StringVar()
    nom_milieu_var.set("Air")
    dic_milieu_frottements = {"Air": 0.002, "Eau":0.2, "Miel":1000}

    # Functions auxiliaires
    def check_float_input(new_input):
        """valid an entry iff it corresponds to a float"""
        try:
            i = float(new_input)
            if i <= 0:
                return False
            return True
        except ValueError:
            return False

    def changement_planete(new_planet):
        intensite_pesanteur_var.set(str(dic_planete_pesanteur[new_planet]))

    def modif_frottements():
        if frottements_var.get():
            frottements_menu.config(state='normal')
        else:
            frottements_menu.config(state='disabled')

    def launch_pygame():
        if frottements_var.get():
            open_initial_window(nom_corps_var.get(), float(intensite_pesanteur_var.get()),
                                float(pos_x_corbeille_var.get()), float(masse_str_var.get()),
                                coeff_frottement=dic_milieu_frottements[nom_milieu_var.get()])
        else:
            open_initial_window(nom_corps_var.get(), float(intensite_pesanteur_var.get()),
                            float(pos_x_corbeille_var.get()), float(masse_str_var.get()))

    # Entry pour la masse
    float_validation_command = (main_window.register(check_float_input), '%P')
    masse_label = Label(main_window, text="Masse (kg)")
    masse_entry = Entry(main_window, textvariable=masse_str_var, validate='all',
                        validatecommand=float_validation_command)
    masse_label.grid(row=1, column=0)
    masse_entry.grid(row=1, column=1)

    # Slider pour la position de la corbeille
    pos_x_corbeille_label = Label(main_window, text="Position de la corbeille (m)")
    pos_x_corbeille_entry = Scale(main_window, orient=HORIZONTAL, length=200,
                                  variable=pos_x_corbeille_var, from_=0., to=300., resolution=0.5, tickinterval=100)
    pos_x_corbeille_label.grid(row=3, column=0)
    pos_x_corbeille_entry.grid(row=3, column=1)

    # Selection de la planète
    corps_label = Label(main_window, text="Corps céleste")
    corps_menu = OptionMenu(main_window, nom_corps_var, "Mercure", "Venus", "Terre", "Mars", "Jupiter", "Saturne",
                            "Uranus", "Neptune", "Pluton", "Lune", "Soleil", "TRAPPIST-1", command=changement_planete)
    corps_label.grid(row=5, column=0)
    corps_menu.grid(row=5, column=1)

    # Label intensité gravité
    intensite_pesanteur_label1 = Label(main_window, text="Intensité de la pesanteur (m/s^2)")
    intensite_pesanteur_label2 = Label(main_window, textvariable=intensite_pesanteur_var)
    intensite_pesanteur_label1.grid(row=6, column=0)
    intensite_pesanteur_label2.grid(row=6, column=1)

    #Checkbox pour les frottements
    frottements_checkbox = Checkbutton(main_window, text = "Ajouter des frottements", variable=frottements_var,
                                       command= modif_frottements)
    frottements_menu = OptionMenu(main_window, nom_milieu_var, "Air", "Eau", "Miel")
    frottements_menu.config(state= "disabled")
    frottements_checkbox.grid(row=7, column=0)
    frottements_menu.grid(row=7, column=1)

    #Button lancement
    lancement_button = Button(main_window, text="Lancer", command=launch_pygame)
    lancement_button.grid(row=8, column=1)

    main_window.mainloop()

if __name__ == '__main__':
    create_main_window()