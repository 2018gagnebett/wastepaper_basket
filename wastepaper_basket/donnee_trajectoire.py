import math

### Fonction qui renvoie la trajectoire sous forme de liste de couple (x,y) en fonction des paramètres

def trajectoire(vitesse, g, theta_radians, position_corbeille, largeur_corbeille, hauteur_corbeille):

    ## Définition des variables
    list_pos=[]  #Liste des couples coordonnées (x,y)
    pas_temporel=0.05 #Pas temporel
    pas_spatial= 0.05 * vitesse  #Pas spatial
    t=0  #Date
    y=0 #Ordonnée à t=0
    temoin=0 #temoin de rebond, voir plus loin explication
    t_impact=0  #Date de l'éventuel impact avec une paroi
    x_impact=0  #Lieu de l'éventuel impact avec une paroi

## On utilise une boucle while avec un système de témoin pour savoir où en est l'objet dans ses divers rebonds
    # temoin==0 : Le projectile va vers la droite (pas de rebond ou rebond sur la 1ere paroi dans la corbeille)
    # temoin==1 : Le projectile va vers la gauche et se situe avant la corbeille (rebond sur la 1ere paroi)
    # temoin==2 : Le projectile va vers la gauche et se situe dans la corbeille (rebond sur la 2eme paroi)

    while y>=0:  #Tant que l'objet n'est pas au sol
        t += pas_temporel
        if temoin == 0:
            x = vitesse * math.cos(theta_radians) * (t - t_impact) + x_impact
            y = vitesse * math.sin(theta_radians) * t - 0.5 * g * t * t
        if (abs(x - position_corbeille)<pas_spatial/2 and y<hauteur_corbeille and temoin==0): #Si l'objet rebondit sur la 1ere paroi à gaauche
            temoin=1
            t_impact=t
            x_impact=x
        if (abs(x-(position_corbeille + largeur_corbeille))<pas_spatial/2 and y<hauteur_corbeille and temoin==0): #Si l'objet rebondit sur la 2eme paroi
            temoin=2
            t_impact=t
            x_impact=x
        if (abs(x - position_corbeille)<pas_spatial/2 and y<hauteur_corbeille and temoin==2):  #Si l'objet rebondit sur la 1ere paroi à droite
            temoin = 0
            t_impact=t
            x_impact=x
        if temoin==1 or temoin==2:
            x = -vitesse * math.cos(theta_radians) * (t - t_impact) + x_impact
            y = vitesse * math.sin(theta_radians) * t - 0.5 * g * t * t
        list_pos.append([x,y])
    return(list_pos)

#fonction test des trajectoires
def test_trajectoire():
    list_pos = trajectoire(10,9.81,math.radians(40),300,60,40)
    theta_radians=math.radians(40)
    x_impact=0
    list_pos_test=[]
    t=0
    t_impact=0
    y = 0
    while y >=0:
        t+=0.05
        x = 10*math.cos(theta_radians)*(t-t_impact)+x_impact
        y = 10*math.sin(theta_radians)*t - 0.5*9.81*t*t
        t_impact=t
        x_impact=x
        list_pos_test.append([x,y])
    test_equality=(list_pos==list_pos_test)
    assert test_equality
### Fonction qui prend en compte les frottements de coefficiant 'a' et qui renvoie
# la trajectoire sous forme de liste de couple (x,y) en fonction des paramètres

def trajectoire_frottements(vitesse, g, theta_radians, position_corbeille, largeur_corbeille, hauteur_corbeille, m, a):

    ## Définition des variables
    list_pos=[]  #Liste des couples coordonnées (x,y)
    pas_temporel=0.05 #Pas temporel
    pas_spatial= 0.05 * vitesse  #Pas spatial
    t=0  #Date
    y=0 #Ordonnée à t=0
    temoin=0 #temoin de rebond, voir plus loin explication
    t_impact=0  #Date de l'éventuel impact avec une paroi
    x_impact=0  #Lieu de l'éventuel impact avec une paroi

## On utilise une boucle while avec un système de témoin pour savoir où en est l'objet dans ses divers rebonds
    # temoin==0 : Le projectile va vers la droite (pas de rebond ou rebond sur la 1ere paroi dans la corbeille)
    # temoin==1 : Le projectile va vers la gauche et se situe avant la corbeille (rebond sur la 1ere paroi)
    # temoin==2 : Le projectile va vers la gauche et se situe dans la corbeille (rebond sur la 2eme paroi)

    while y>=0:  #Tant que l'objet n'est pas au sol
        t += pas_temporel
        if temoin == 0: #Si l'objet va vers la droite
            x = m / a * vitesse * math.cos(theta_radians) * (1 - math.exp(-a * t / m))
            y = m / a * vitesse * math.sin(theta_radians) * (1 - math.exp(-a * t / m)) + g * (m / a) ** 2 * (1 - a * t / m - math.exp(-a * t / m))
        if (abs(x - position_corbeille)<pas_spatial/2 and y<hauteur_corbeille and temoin==0): #Si l'objet rebondit sur la 1ere paroi à gaauche
            temoin=1
            t_impact=t
            x_impact=x
        if (abs(x-(position_corbeille + largeur_corbeille))<pas_spatial/2 and y<hauteur_corbeille and temoin==0): #Si l'objet rebondit sur la 2eme paroi
            temoin=2
            t_impact=t
            x_impact=x
        if (abs(x - position_corbeille)<pas_spatial/2 and y<hauteur_corbeille and temoin==2):  #Si l'objet rebondit sur la 1ere paroi à droite
            temoin = 0
            t_impact=t
            x_impact=x
        if temoin==1 or temoin==2: #Si l'objet vas vers la gauche
            x = -m / a * vitesse * math.cos(theta_radians) * (1 - math.exp(-a * (t - t_impact) / m)) + x_impact
            y = m / a * vitesse * math.sin(theta_radians) * (1 - math.exp(-a * t / m)) + g * (m / a) ** 2 * (1 - a * t / m - math.exp(-a * t / m))
        list_pos.append([x,y])
    return(list_pos)
