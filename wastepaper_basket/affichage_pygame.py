import pygame
from math import atan2, hypot, cos, sin, floor
from wastepaper_basket.donnee_trajectoire import trajectoire, trajectoire_frottements

win_size = (400, 300)
sol_size = (400, 50)
base_fleche = (20, 250)
x_panier = 300
dimension_panier = (60, 40)
#variables globales qui représentent le décor de la fenêtre
decor_surface = None
sol_surface = None
panier_surface = None

def surface_arrow(angle, distance):
    """Renvoie la surface représentant la flèche."""
    longueur_rect = int(max(0., distance - 15))
    fleche_surface = pygame.Surface(win_size)
    fleche_surface.set_colorkey((0, 0, 0))
    pt1 = (base_fleche[0] - 5 * sin(angle), base_fleche[1] + 5 * cos(angle))
    pt2 = (base_fleche[0] + 5 * sin(angle), base_fleche[1] - 5 * cos(angle))
    pt3 = (pt2[0] + longueur_rect * cos(angle), pt2[1] + longueur_rect * sin(angle))
    pt4 = (pt1[0] + longueur_rect * cos(angle), pt1[1] + longueur_rect * sin(angle))
    pt5 = (pt3[0] + 5 * sin(angle), pt3[1] - 5 * cos(angle))
    pt6 = (pt4[0] - 5 * sin(angle), pt4[1] + 5 * cos(angle))
    pt7 = (base_fleche[0] + (longueur_rect + 15) * cos(angle), base_fleche[1] + (longueur_rect + 15) * sin(angle))
    pygame.draw.polygon(fleche_surface, (255, 0, 0),
                        [pt1, pt2, pt3, pt4])
    pygame.draw.polygon(fleche_surface, (255, 0, 0),
                        [pt5, pt6, pt7])
    return fleche_surface

def build_decor(planete):
    """Remplis les surfaces "decor_surface" et "sol_surface" en fonction de la planète avec les images associées"""
    global decor_surface
    global sol_surface
    decor_surface = pygame.Surface(win_size)
    sol_surface = pygame.Surface(sol_size)

    if planete == "Mercure":
        decor_surface.fill((0, 0, 0))
        image_sol = pygame.image.load("Images/sol-Mercure.png")
        sol_surface.blit(pygame.transform.scale(image_sol, win_size), (0, 0))
    elif planete == "Venus":
        image_decor = pygame.image.load("Images/image-Venus.png")
        decor_surface.blit(pygame.transform.scale(image_decor, win_size), (0, 0))
        sol_surface.set_colorkey((0, 0, 0))
    elif planete == "Terre":
        image_decor = pygame.image.load("Images/photographier-la-lune.jpg")
        decor_surface.blit(image_decor, (0, 0))
        sol_surface.fill((0, 0, 0))
    elif planete == "Mars":
        image_decor = pygame.image.load("Images/Atmosphere_mars.png")
        decor_surface.blit(pygame.transform.scale(pygame.transform.scale(image_decor, win_size),
                                                  (win_size[0], win_size[1] - sol_size[1])), (0, 0))
        image_sol = pygame.image.load("Images/Sol_martien.jpg")
        sol_surface.blit(pygame.transform.scale(image_sol, sol_size), (0, 0))
    elif planete == "Jupiter":
        decor_surface.fill((254, 163, 71))
        sol_surface.fill((179, 103, 0))
    elif planete == "Saturne":
        image_decor = pygame.image.load("Images/atmosphere-saturne.jpg")
        decor_surface.blit(pygame.transform.scale(image_decor, win_size), (0, 0))
        sol_surface.set_colorkey((0, 0, 0))
    elif planete == "Uranus":
        decor_surface.fill((204, 204, 255))
        sol_surface.fill((223, 242, 255))
    elif planete == "Neptune":
        decor_surface.fill((119, 181, 254))
        sol_surface.fill((21, 96, 189))
    elif planete == "Pluton":
        decor_surface.fill((0, 0, 0))
        image_sol = pygame.image.load("Images/sol-pluton.png")
        sol_surface.blit(pygame.transform.scale(image_sol, sol_size), (0, 0))
    elif planete == "Lune":
        image_decor = pygame.image.load("Images/Terre_vue_depuis_lune.png")
        decor_surface.blit(pygame.transform.scale(image_decor, (win_size[0], win_size[1] - sol_size[1])), (0, 0))
        image_sol = pygame.image.load("Images/sol-lunaire.jpg")
        sol_surface.blit(pygame.transform.scale(image_sol, sol_size), (0, 0))
    elif planete == "Soleil":
        decor_surface.fill((255, 255, 5))
        sol_surface.fill((243, 214, 23))
    elif planete == "TRAPPIST-1":
        decor_surface.fill((255, 255, 5))
        sol_surface.fill((243, 214, 23))
    else:
        decor_surface.fill((0, 0, 125))
        sol_surface.fill((0, 125, 0))

def build_bravo():
    """Renvoie la surface contenant le texte à afficher en cas de réussite"""
    pygame.font.init()
    used_font = pygame.font.SysFont("Calibri", 60, bold= True)
    bravo_surface = used_font.render("BRAVO !", True, (255, 255, 255))
    pygame.font.quit()
    return bravo_surface

def build_basket(planete):
    """Remplis la surfaces "panier_surface" en fonction de la planète avec la couleur associée"""
    global panier_surface
    panier_surface = pygame.Surface(dimension_panier)
    panier_surface.set_colorkey((0, 0, 0))
    panier_couleur = (125, 0, 0)
    if planete == "Saturne":
        panier_couleur = (138, 46, 228)
    pygame.draw.rect(panier_surface, panier_couleur, (0, 0, 5, dimension_panier[1]))  # bord gauche
    pygame.draw.rect(panier_surface, panier_couleur, (0, dimension_panier[1] - 5, dimension_panier[0], 5))  # fond
    pygame.draw.rect(panier_surface, panier_couleur, (dimension_panier[0] - 5, 0, 5, dimension_panier[1]))  # bord droit

def draw_decor(screen):
    """Représente le décor dans la fenêtre à partir des surface "decor_surface" et "sol_surface"""
    screen.blit(decor_surface, (0, 0))
    screen.blit(sol_surface, (0, win_size[1] - 50))

def draw_basket(screen):
    """Dessine le panier"""
    screen.blit(panier_surface, (x_panier + base_fleche[0], base_fleche[1] - dimension_panier[1]))

def draw_screen_arrow(screen, fleche_angle, fleche_distance):
    """Dessine et affiche le décor et la flèche"""
    # Dessine le décor et le panier
    draw_decor(screen)
    draw_basket(screen)

    # Dessine la flèche
    fleche_surface = surface_arrow(-fleche_angle, fleche_distance)
    screen.blit(fleche_surface, (0, 0))

    # Affiche le décor, le panier et la flèche
    pygame.display.flip()

def draw_screen_ball(screen, x, y):
    """Dessine et affiche le décor et la balle"""
    # Dessine le décor
    draw_decor(screen)

    #Dessine la boulette
    balle_surface = pygame.Surface((10, 10))
    balle_surface.set_colorkey((0, 0, 0))
    pygame.draw.circle(balle_surface, (255, 255, 255), (5, 5), 5)
    screen.blit(balle_surface, (x + base_fleche[0], -y + base_fleche[1] - 10))

    #Dessine le panier
    draw_basket(screen)

    # Affiche l'ensemble
    pygame.display.flip()

def open_initial_window(nom_corps, g, pos_x_panier, masse, coeff_frottement=0.):
    """lance la fenêtre du jeu"""
    # Define variables
    global x_panier
    global win_size
    global sol_size
    global base_fleche
    x_panier = int(pos_x_panier)

    # Initialise pygame
    pygame.init()
    screen = pygame.display.set_mode(win_size, pygame.RESIZABLE)
    pygame.display.set_caption("Wastepaper Basket")

    # Charge l'image s'affichant si l'utilisateur gagne
    bravo_surface = build_bravo()

    # Définition des variables
    angle = 0.
    taille_fleche = 55.
    is_playing = False
    tableau_positions = []
    i = 0

    # Dessine l'écran initial
    build_decor(nom_corps)
    build_basket(nom_corps)
    draw_screen_arrow(screen, angle, taille_fleche)

    # La boucle
    should_continue = True
    while should_continue:
        for event in pygame.event.get():
            # Check if quit
            if event.type == pygame.QUIT:
                should_continue = False
                break
            # Modifie l'angle initial en fonction de la position de la souris
            if event.type == pygame.MOUSEMOTION and not is_playing:
                mouse_position = event.pos
                angle = atan2(-mouse_position[1] + base_fleche[1], mouse_position[0] - base_fleche[0])
                taille_fleche = hypot(-mouse_position[1] + base_fleche[1], mouse_position[0] - base_fleche[0])
                draw_screen_arrow(screen, angle, taille_fleche)
            # Lance le jeu (la vitesse initiale est définie à partir de la taille de la flèche "rayon")
            if event.type == pygame.MOUSEBUTTONUP and not is_playing:
                is_playing = True
                if coeff_frottement == 0.:
                    tableau_positions = trajectoire(taille_fleche/3., g, angle, x_panier,
                                                dimension_panier[0], dimension_panier[1])
                else:
                    tableau_positions = trajectoire_frottements(taille_fleche/3., g, angle, x_panier,
                                                dimension_panier[0], dimension_panier[1], masse, coeff_frottement)
                i = 0
                # une image est affichée toutes les 10 millisecondes à la réception de l'événement de type USEREVENT
                pygame.time.set_timer(pygame.USEREVENT, 10)
            if event.type == pygame.USEREVENT and is_playing:
                if i < len(tableau_positions):
                    # Afficher une nouvelle image de la balle
                    draw_screen_ball(screen, tableau_positions[i][0], tableau_positions[i][1])
                    i += 1
                else:
                    # Il n'y a plus de points à afficher
                    pygame.time.set_timer(pygame.USEREVENT, 0)
                    if tableau_positions[-1][0]>x_panier and tableau_positions[-1][0]<(x_panier+dimension_panier[0]):
                        # Victoire
                        screen.blit(bravo_surface, ((win_size[0] - bravo_surface.get_width())/2,
                                                    (win_size[1] - bravo_surface.get_height())/2))
                        pygame.display.flip()
                    pygame.time.set_timer(pygame.USEREVENT + 1, 500)  # L'affichage de la dernière image dure 0,5s
            # Il s'agit de la fin de l'affichage de la dernière image
            if event.type == pygame.USEREVENT+1:
                is_playing = False
                pygame.time.set_timer(pygame.USEREVENT+1, 0)
                draw_screen_arrow(screen, angle, taille_fleche)
    pygame.quit()

if __name__ == '__main__':
    open_initial_window("Terre", 9.81, 300, 1.)
