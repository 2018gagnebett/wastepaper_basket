
#Ceci est le code commun et il marche
from matplotlib import pyplot as plt
from matplotlib import animation
import math



g = 9.8
u = 30	# initial speed
theta = 50	# Initial launch angle
h = 0 #hauteur du lancer*
l_c=10
h_c=20
x_c=60 #position corbeille

temoin = 0
t_impact=0
x_impact=0
def parameters(u,g,theta_radians,h):
    t_flight= (u*math.sin(theta_radians)+math.sqrt(u**2*math.sin(theta_radians)**2+2*g*h))/g		# Time from point A to point B
    t_max = u*math.sin(theta_radians)/g	# Time required to rise to maximum height
    xmax = u*math.cos(theta_radians)*t_flight  # Distance covered by the ball
    ymax = u*math.sin(theta_radians)*t_max - 0.5*g*t_max**2+h  # Maximum height of rise
    return (xmax, ymax, t_flight)


def trajectoire_animee(u,g,theta,h,x_c): #u=vitesseini, theta=angle(degrés), h=hauteur de lancer, x_c=abscisse corbeille
    # Initialize the graphics window
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    theta_radians=math.radians(theta)
    xmin_fenetre = 0
    ymin_fenetre = 0
    xmax,ymax = parameters(u,g,theta_radians,h)[0:2]
    xmax_fenetre, ymax_fenetre=x_c+20,ymax+10
    ymax_fenetre= ymax+10
    ax = plt.axes(xlim=(xmin_fenetre,xmax_fenetre),ylim=(ymin_fenetre,ymax_fenetre))
    # Create a circle with a dot at (0,0) and a radius of 1
    circle = plt.Circle((0, 0), 1)
    ax.add_patch(circle)
    t_flight=parameters(u,g,theta_radians,h)[2]
    def generate():
        t=0
        while y>=0:
            t += 0.05
            yield t
    epsilon=0.05*u/2
    y=h
    def update(t):
        global x
        global y
        global x_impact
        global t_impact
        global temoin
        if temoin == 0:
            x = u*math.cos(theta_radians)*(t-t_impact)+x_impact
            y = u*math.sin(theta_radians)*t - 0.5*g*t*t+h
            circle.center = x, y
        if (abs(x-(x_c+l_c))<epsilon and y<h_c and temoin==0):
            temoin=3
            t_impact=t
            x_impact=x
        if (abs(x-x_c)<epsilon and y<h_c and temoin==0):
            temoin=1
            t_impact=t
            x_impact=x
        if (abs(x-x_c)<epsilon and y<h_c and temoin==3):
            temoin = 0
            t_impact=t
            x_impact=x
        if temoin==1 or temoin==3:
            x = -u*math.cos(theta_radians)*(t-t_impact)+x_impact
            y = u*math.sin(theta_radians)*t - 0.5*g*t*t+h
            circle.center = x, y
        return circle,
    anim= animation.FuncAnimation(fig, update, generate,interval=10)
    plt.title(u'Missile launch trajectory')
    plt.xlabel(u'Horizontal distance (m)')
    plt.ylabel(u'Missile running height (m)')
    plt.plot([x_c,x_c+l_c],[0,0],'-b',lw=2) #représentation de la corbeille de largeur 10 et hauteur 5
    plt.plot([x_c,x_c],[0,h_c],'-b',lw=2)
    plt.plot([x_c+l_c,x_c+l_c],[0,h_c],'-b',lw=2)
    plt.show()



trajectoire_animee(u,g,theta,h,x_c)
